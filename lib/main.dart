import 'package:contacter_app/screens/history_screen/screen.dart';
import 'package:contacter_app/screens/home_screen/screen.dart';
import 'package:contacter_app/screens/home_screen/widgets/home_page_loaded.dart';
import 'package:contacter_app/screens/settings_screen/screen.dart';
import 'package:contacter_app/screens/subscription_screen/screen.dart';
import 'package:contacter_app/theme/color_theme.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Montserrat'),
      title: 'Contacts Backup - CONTACTER',
      color: ColorPalette.white,
      home: HomePage(),
    );
  }
}
