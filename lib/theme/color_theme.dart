import 'package:flutter/material.dart';

class ColorPalette {
  static const light_purple = Color(0xFF6465F9);
  static const purple = Color(0xFF4D4AF9);
  static const teal = Color(0xFF56F3C4);
  static const white = Color(0xFFFFFFFF);
  static const black = Color(0xFF000000);
  static const grey = Color(0xFF8D8D8D);
  static const pale_purple = Color(0xFFC5C4FE);
}