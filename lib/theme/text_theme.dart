import 'package:contacter_app/theme/color_theme.dart';
import 'package:flutter/material.dart';

class TextThemes {
  static const title = TextStyle(
    fontSize: 25.0,
    fontWeight: FontWeight.w700,
    height: 1.2,
  );

  static const title_2 = TextStyle(
    fontSize: 28.0,
    fontWeight: FontWeight.w900,
    height: 1.28,
    color: ColorPalette.white,
  );

  static const title_3 = TextStyle(
    fontSize: 24.0,
    fontWeight: FontWeight.w500,
    height: 1.22,
  );

  static const title_4 = TextStyle(
    fontSize: 32.0,
    fontWeight: FontWeight.w700,
    height: 1.22,
    color: ColorPalette.white,
  );

  static const title_5 = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
    height: 1.5,
  );

  static const title_6 = TextStyle(
    fontSize: 15.0,
    fontWeight: FontWeight.w700,
    height: 1.22,
    color: ColorPalette.white,
  );

  static const title_7 = TextStyle(
    fontSize: 10.0,
    fontWeight: FontWeight.w500,
    height: 1.2,
    color: ColorPalette.light_purple,
  );

  static const title_8 = TextStyle(
    fontSize: 10.0,
    fontWeight: FontWeight.w500,
    height: 1.2,
    color: ColorPalette.pale_purple,
  );

  static const body = TextStyle(
    fontSize: 15.0,
    fontWeight: FontWeight.w600,
    height: 1.2,
  );

  static const body_2 = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
    height: 1.42,
    color: ColorPalette.grey,
  );
  
  static const body_3 = TextStyle(
    fontSize: 23.0,
    fontWeight: FontWeight.w700,
    height: 1.21,
    color: ColorPalette.light_purple,
  );

  static const body_4 = TextStyle(
    fontSize: 17.0,
    fontWeight: FontWeight.w700,
    height: 1.41,
  );

  static const subtitle = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
    height: 1.71,
    color: ColorPalette.light_purple,
  );

  static const subtitle_2 = TextStyle(
    fontSize: 13.0,
    fontWeight: FontWeight.w500,
    height: 1.23,
    // color: ColorPalette.black.withOpacity(0.6),
  );

  
}
