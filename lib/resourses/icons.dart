class MainIcons {
  static String get close => 'assets/svg_icons/close.svg';
  static String get loop => 'assets/svg_icons/loop.svg';
  static String get cloud => 'assets/svg_icons/cloud.svg';
  static String get shield => 'assets/svg_icons/shield.svg';
  static String get cloud_2 => 'assets/svg_icons/cloud_2.svg';
  static String get share => 'assets/svg_icons/share.svg';
  static String get history => 'assets/svg_icons/history.svg';
  static String get backup => 'assets/svg_icons/backup.svg';
  static String get settings => 'assets/svg_icons/settings.svg';
  static String get history_active => 'assets/svg_icons/history_active.svg';
  static String get backup_active => 'assets/svg_icons/backup_active.svg';
  static String get settings_active => 'assets/svg_icons/settings_active.svg';
}