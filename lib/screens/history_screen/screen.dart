import 'package:contacter_app/screens/history_screen/widgets/history_item.dart';
import 'package:contacter_app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class HistoryPage extends StatelessWidget {
  final historyList;

  HistoryPage({this.historyList});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Text(
              'History',
              style: TextThemes.title,
            ),
            ListView.builder(
              itemCount: historyList.length,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, index) {
                return HistoryItem(
                  historyItem: historyList[index],
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
