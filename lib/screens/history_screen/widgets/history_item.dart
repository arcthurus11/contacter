import 'package:contacter_app/resourses/icons.dart';
import 'package:contacter_app/screens/models/history_model.dart';
import 'package:contacter_app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HistoryItem extends StatelessWidget {
  final HistoryModel historyItem;

  HistoryItem({this.historyItem});
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      const Divider(
              thickness: 1,
            ),
            ListTile(
              contentPadding: EdgeInsets.only(left: 24.0, right: 29.0),
              leading: SvgPicture.asset(MainIcons.cloud_2),
              trailing: SvgPicture.asset(MainIcons.share),
              title: Text('${historyItem.date}', style: TextThemes.body_4),
              subtitle: Row(
                children: <Widget>[
                  Text(
                    '${historyItem.time} • ',
                    style: TextThemes.subtitle_2,
                  ),
                  Text(
                    '${historyItem.contactsCount} contacts • ',
                    style: TextThemes.subtitle_2,
                  ),
                  Text(
                    '${historyItem.size}kb',
                    style: TextThemes.subtitle_2,
                  ),
                ],
              ),
            ),
    ],);
  }
}