import 'package:contacter_app/resourses/icons.dart';
import 'package:contacter_app/screens/history_screen/screen.dart';
import 'package:contacter_app/screens/home_screen/widgets/home_page_body.dart';
import 'package:contacter_app/screens/home_screen/widgets/home_page_content.dart';
import 'package:contacter_app/screens/home_screen/widgets/home_page_loaded.dart';
import 'package:contacter_app/screens/settings_screen/screen.dart';
import 'package:contacter_app/theme/color_theme.dart';
import 'package:contacter_app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import 'bloc/contacts_bloc.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String title;
  String text;

  int currentIndex = 1;

  final List<Widget> screens = [
    HistoryPage(),
    HomePageBody(),
    SettingsPage(),
  ];

  @override
  void initState() {
    super.initState();
    title = 'Contacts Back up';
    text = 'Backup your address book to avoid losing your contacts in case something happens to your phone.';
  }

  void onTapped(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: ColorPalette.light_purple,
        unselectedItemColor: ColorPalette.pale_purple,
        selectedLabelStyle: TextThemes.title_7,
        unselectedLabelStyle: TextThemes.title_8,
        currentIndex: currentIndex,
        onTap: onTapped,
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(MainIcons.history),
            activeIcon: SvgPicture.asset(MainIcons.history_active),
            label: 'History',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(MainIcons.backup),
            activeIcon: SvgPicture.asset(MainIcons.backup_active),
            label: 'Back up',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(MainIcons.settings),
            activeIcon: SvgPicture.asset(MainIcons.settings_active),
            label: 'Settings',
          ),
        ],
      ),
      body: currentIndex == 1
          ? HomePageBody(
              title: title,
              text: text,
            )
          : screens[currentIndex],
    );
  }
}
