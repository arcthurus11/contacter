import 'package:flutter/material.dart';
import 'package:flutter_contacts/flutter_contacts.dart';

abstract class ContactsState {}

class ContactsEmptyState extends ContactsState {}

class ContactsLoadingState extends ContactsState {}

class ContactsLoadedState extends ContactsState {
  List<Contact> contacts;
  ContactsLoadedState({@required this.contacts}) : assert(contacts != null);
}

class ContactsErrorState extends ContactsState {}