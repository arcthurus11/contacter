import 'package:contacter_app/screens/home_screen/bloc/contacts_event.dart';
import 'package:contacter_app/screens/home_screen/bloc/contacts_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_contacts/flutter_contacts.dart';

class ContactsBloc extends Bloc<ContactsEvent, ContactsState> {
  ContactsBloc() : super(ContactsEmptyState());

  @override
  Stream<ContactsState> mapEventToState(ContactsEvent event) async* {
    if (event is ContactsGetEvent) {
      yield ContactsLoadingState();
      try {
        final List<Contact> _loadedContacts = await FlutterContacts.getContacts();
        yield ContactsLoadedState(contacts: _loadedContacts);
      } catch(_) {
        yield ContactsErrorState();
      }
    }
  }
}