import 'package:contacter_app/resourses/images.dart';
import 'package:contacter_app/theme/color_theme.dart';
import 'package:contacter_app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class HomePageLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 40.0, top: 32.0),
              child: Text(
                'Contacts Backup',
                style: TextThemes.title,
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(bottom: 83.0, left: 24.0, right: 24.0),
              child: Text(
                'Backup your address book to avoid losing your contacts in case something happens to your phone.',
                style: TextThemes.body,
                textAlign: TextAlign.center,
              ),
            ),
            Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 87.0),
                  child: Align(
                    alignment: Alignment(0.0, 0.0),
                    child: Container(
                      height: 442.0,
                      width: 375.0,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(Images.background),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 283.0,
                  left: 65.0,
                  child: Center(
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      height: 246.0,
                      width: 246.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            ColorPalette.purple,
                            ColorPalette.teal,
                          ],
                        ),
                      ),
                      child: Center(
                        child: Text(
                          'Create \nBackup',
                          style: TextThemes.title_2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}