import 'package:contacter_app/resourses/images.dart';
import 'package:contacter_app/screens/home_screen/bloc/contacts_bloc.dart';
import 'package:contacter_app/screens/home_screen/bloc/contacts_state.dart';
import 'package:contacter_app/screens/home_screen/widgets/home_page_body.dart';
import 'package:contacter_app/screens/home_screen/widgets/home_page_loading.dart';
import 'package:contacter_app/theme/color_theme.dart';
import 'package:contacter_app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePageContent extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    
    return HomePageBody();

    
  }
}
