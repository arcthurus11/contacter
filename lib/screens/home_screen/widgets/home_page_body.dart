import 'package:contacter_app/resourses/images.dart';
import 'package:contacter_app/screens/home_screen/bloc/contacts_bloc.dart';
import 'package:contacter_app/screens/home_screen/bloc/contacts_event.dart';
import 'package:contacter_app/theme/color_theme.dart';
import 'package:contacter_app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_contacts/config.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:flutter_contacts/vcard.dart';

class HomePageBody extends StatefulWidget {
  final String title;
  final String text;
  HomePageBody({this.title, this.text});

  @override
  _HomePageBodyState createState() => _HomePageBodyState();
}

class _HomePageBodyState extends State<HomePageBody> {
  List <Contact> _contacts;
  bool _permissionDenied = false;
  
  Future _fetchContacts () async {
    if (!await FlutterContacts.requestPermission()) {
      setState(() => _permissionDenied = true);
    } else {
      final contacts = await FlutterContacts.getContacts();
      setState(() => _contacts = contacts);
    }
    List<dynamic> vCardContacts;
    for (int i; i <=_contacts.length; i++)
    vCardContacts.add(_contacts[i].toVCard());

    return vCardContacts;
  }

  @override
  Widget build(BuildContext context) {
    // final ContactsBloc contactsBloc = BlocProvider.of<ContactsBloc>(context);
    return SafeArea(
      bottom: false,
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 40.0, top: 32.0),
              child: Text(
                widget.title,
                style: TextThemes.title,
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(bottom: 83.0, left: 24.0, right: 24.0),
              child: Text(
                widget.text,
                style: TextThemes.body,
                textAlign: TextAlign.center,
              ),
            ),
            Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 87.0),
                  child: Align(
                    alignment: Alignment(0.0, 0.0),
                    child: Container(
                      height: 442.0,
                      width: 375.0,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(Images.background),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 283.0,
                  left: 65.0,
                  child: Center(
                    child: GestureDetector(
                      onTap: () {
                        _fetchContacts();
                        // contactsBloc.add(ContactsGetEvent(),);
                      },
                      child: Container(
                        padding: EdgeInsets.all(5.0),
                        height: 246.0,
                        width: 246.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              ColorPalette.purple,
                              ColorPalette.teal,
                            ],
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'Create \nBackup',
                            style: TextThemes.title_2,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
