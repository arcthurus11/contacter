import 'package:contacter_app/theme/color_theme.dart';
import 'package:contacter_app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class HomePageLoaded extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(color: ColorPalette.black),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 32.0, bottom: 40.0),
              child: Text(
                'Completed',
              ),
            ),
            Text(
              'Your backup has been completed and saved to your archive.',
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 196.5, bottom: 215.5),
              child: Text('100%'),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: 0.0, bottom: 0.0, right: 24.0, left: 24.0),
              child: Container(
                color: ColorPalette.white,
                height: 56.0,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: ColorPalette.white,
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                child: OutlineButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Text(
                    'Open Backup',
                    style: TextThemes.title_6,
                  ),
                  onPressed: () {},
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
