import 'package:contacter_app/resourses/icons.dart';
import 'package:contacter_app/screens/subscription_screen/widgets/gradient.dart';
import 'package:contacter_app/theme/color_theme.dart';
import 'package:contacter_app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SubscriptionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leadingWidth: 100.0,
        leading: RawMaterialButton(
          padding: EdgeInsets.all(0.0),
          child: SvgPicture.asset(
            MainIcons.close,
            width: 12.0,
            height: 12.0,
          ),
          shape: CircleBorder(),
          fillColor: ColorPalette.black.withOpacity(0.4),
          onPressed: () {},
        ),
        title: Align(
          alignment: Alignment.centerRight,
          child: GestureDetector(child: Text('Restore Purchases', style: TextThemes.subtitle)),
        ),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 24.0),
            child: Text('Contacts Backup', style: TextThemes.title_3),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 16.0),
            child: GradientText(
                text: 'Pro',
                gradient: LinearGradient(
                    colors: [ColorPalette.teal, ColorPalette.purple]),
                style: TextThemes.title_4),
          ),
          Padding(
            padding: const EdgeInsets.only(
                right: 24.0, left: 24.0, bottom: 32.0),
            child: Text(
              'We understand that your contacts are important for you, so we have developed Contact Backup Master to keep your address book secure at all times',
              style: TextThemes.body_2,
              textAlign: TextAlign.center,
            ),
          ),
          ListTile(
            leading: SvgPicture.asset(MainIcons.loop),
            title: Transform.translate(
              offset: Offset(-16, 3),
              child: Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text('Automatic Backups', style: TextThemes.title_5),
              ),
            ),
            subtitle: Transform.translate(
              offset: Offset(-16, 3),
              child: Text('Automatically back up your contacts',
                  style: TextThemes.body_2),
            ),
            contentPadding: EdgeInsets.only(left: 24.0, top: 0, bottom: 0),
          ),
          ListTile(
            leading: SvgPicture.asset(MainIcons.cloud),
            title: Transform.translate(
              offset: Offset(-16, 3),
              child: Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text('Cloud Storage', style: TextThemes.title_5),
              ),
            ),
            subtitle: Transform.translate(
              offset: Offset(-16, 3),
              child: Text('Store your backups securely in the cloud',
                  style: TextThemes.body_2),
            ),
            contentPadding: EdgeInsets.only(left: 24.0, top: 0, bottom: 0, right: 24.0),
          ),
          ListTile(
            leading: SvgPicture.asset(MainIcons.shield),
            title: Transform.translate(
              offset: Offset(-16, 10),
              child: Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text('Backups Protection', style: TextThemes.title_5),
              ),
            ),
            subtitle: Transform.translate(
              offset: Offset(-16, 10),
              child: Text('Your cloud backups will be securely protected',
                  style: TextThemes.body_2),
            ),
            contentPadding: EdgeInsets.only(left: 24.0, top: 0, bottom: 50.0),
          ),
          Text(
            'Free unlimited access',
            style: TextThemes.body_2,
          ),
          Padding(
            padding: EdgeInsets.only(
                top: 16.0, bottom: 0.0, right: 24.0, left: 24.0),
            child: Container(
              height: 56.0,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                gradient: LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: [
                    ColorPalette.teal,
                    ColorPalette.purple,
                  ],
                ),
              ),
              child: OutlineButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Text(
                  'Get Master version',
                  style: TextThemes.title_6,
                ),
                onPressed: () {},
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 24.0, right: 24.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                GestureDetector(
                  child: Text(
                    'Terms of Use',
                    style: TextThemes.subtitle,
                  ),
                ),
                GestureDetector(
                  child: Text(
                    'Privacy policy',
                    style: TextThemes.subtitle,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
