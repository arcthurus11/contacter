class HistoryModel {
  final String date;
  final String time;
  final int contactsCount;
  final int size;

  HistoryModel({this.date, this.time, this.contactsCount, this.size});
}