import 'package:contacter_app/resourses/images.dart';
import 'package:contacter_app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class SettingsContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        Positioned(
          bottom: 0,
          child: Center(
            child: Container(
              height: 638.57,
              width: 375,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(Images.background_1),
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 172.0, top: 32.0),
              child: Text(
                'Settings',
                style: TextThemes.title,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 48.0),
              child: Text(
                'Help',
                style: TextThemes.body_3,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 48.0),
              child: Text(
                'Privacy Policy',
                style: TextThemes.body_3,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 48.0),
              child: Text(
                'Terms of Use',
                style: TextThemes.body_3,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 48.0),
              child: Text(
                'Support',
                style: TextThemes.body_3,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
