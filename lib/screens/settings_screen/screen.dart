import 'package:contacter_app/screens/settings_screen/widgets/settings_content.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: SettingsContent(),
      ),
    );
  }
}
